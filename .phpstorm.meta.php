<?php
namespace PHPSTORM_META;

// Hint that makeInstance will return an instance of the class it gets passed
override(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(0), type(0));

// Hint that object manager will return an instance of the class it gets passed
override(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface::get(0), type(0));

override(\PHPUnit\Framework\TestCase::createMock(0), type(0));
override(\PHPUnit\Framework\TestCase::createConfiguredMock(0), type(0));
override(\PHPUnit\Framework\TestCase::createPartialMock(0), type(0));
override(\PHPUnit\Framework\TestCase::createTestProxy(0), type(0));
override(\PHPUnit\Framework\TestCase::getMockForAbstractClass(0), type(0));

// requires the deep-assoc-completion extension
$GLOBALS['TSFE'] = new \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController();
$GLOBALS['TYPO3_DB'] = new \TYPO3\CMS\Core\Database\DatabaseConnection();
