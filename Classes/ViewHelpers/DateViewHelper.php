<?php

namespace Hn\Intl\ViewHelpers;


use Hn\Intl\Configuration;
use IntlDateFormatter;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class DateViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper
{
    private const DAY = '(?:[dj]|%[de])';
    private const MONTH_SHORT = '(?:[Mmn]|%[bhm])';
    private const MONTH_LONG = '(?:F|%B)';
    private const MONTH_ANY = '(?:[MmnF]|%[bhmB])';
    private const WEEKDAY = '(?:[DlNw]|%[aA])';
    private const DAY_MONTH_SHORT = '(?:' . self::DAY . '\\W+' . self::MONTH_SHORT . '|' . self::MONTH_SHORT . '\\W+' . self::DAY . ')';
    private const DAY_MONTH_MEDIUM = '(?:' . self::DAY . '\\W+' . self::MONTH_LONG . '|' . self::MONTH_LONG . '\\W+' . self::DAY . ')';
    private const DAY_MONTH_LONG = '(?:' . self::WEEKDAY . '\\W+' . self::DAY . '\\W+' . self::MONTH_ANY . '|' . self::WEEKDAY . '\\W+' . self::MONTH_ANY . '\\W+' . self::DAY . ')';

    private const YEAR_SHORT = '(?:y|%y)';
    private const YEAR_LONG = '(?:Y|%Y)';
    private const YEAR_ANY = '(?:[yY]|%[yY])';

    private const HOUR = '(?:[gGhH]|%[HkIl])';
    private const MINUTE = '(?:i|%M)';
    private const HOUR_MINUTE = '(?:' . self::HOUR . '\\W+' . self::MINUTE . '|%R)';
    private const SECOND = '(?:s|%S)';
    private const OPTIONAL_AM_PM = '(?:[aA]|%[pP])?';

    private const DATE_SHORT = '(?:' . self::DAY_MONTH_SHORT . '\\W+' . self::YEAR_SHORT . '|' . self::YEAR_SHORT . '\\W+' . self::DAY_MONTH_SHORT . '|%[DFx])';
    private const DATE_MEDIUM = '(?:' . self::DAY_MONTH_SHORT . '\\W+' . self::YEAR_LONG . '|' . self::YEAR_LONG . '\\W+' . self::DAY_MONTH_SHORT . ')';
    private const DATE_LONG = '(?:' . self::DAY_MONTH_MEDIUM . '\\W+' . self::YEAR_ANY . '|' . self::YEAR_ANY . '\\W+' . self::DAY_MONTH_MEDIUM . ')';
    private const DATE_FULL = '(?:' . self::DAY_MONTH_LONG . '\\W+' . self::YEAR_ANY . '|' . self::YEAR_ANY . '\\W+' . self::DAY_MONTH_LONG . ')';

    private const TIME_SHORT = '(?:' . self::HOUR_MINUTE . '\\W*' . self::OPTIONAL_AM_PM . '|%[R])';
    private const TIME_MEDIUM = '(?:' . self::HOUR_MINUTE . '\\W+' . self::SECOND . '\\W*' . self::OPTIONAL_AM_PM . '|%[rTX])';
    private const TIME_LONG = self::HOUR_MINUTE . '\\W+' . self::SECOND . '\\W*' . self::OPTIONAL_AM_PM . '\\W+(?:[OPT]|%[Zz])';
    private const TIME_FULL = self::HOUR_MINUTE . '\\W+' . self::SECOND . '\\W*' . self::OPTIONAL_AM_PM . '\\W+e';

    /**
     * The expression that maps the original date format to intl date formatter options.
     * This is inspired by symfony's router using markers.
     * @see https://symfony.com/blog/new-in-symfony-4-1-fastest-php-router
     * @see http://www.rexegg.com/backtracking-control-verbs.html#mark
     */
    private const EXPRESSION = '#^(?'

    . '|' . self::DATE_SHORT . '(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::NONE . ')'
    . '|' . self::DATE_MEDIUM . '(*:' . IntlDateFormatter::MEDIUM . ',' . IntlDateFormatter::NONE . ')'
    . '|' . self::DATE_LONG . '(*:' . IntlDateFormatter::LONG . ',' . IntlDateFormatter::NONE . ')'
    . '|' . self::DATE_FULL . '(*:' . IntlDateFormatter::FULL . ',' . IntlDateFormatter::NONE . ')'

    . '|' . self::TIME_SHORT . '(*:' . IntlDateFormatter::NONE . ',' . IntlDateFormatter::SHORT . ')'
    . '|' . self::TIME_MEDIUM . '(*:' . IntlDateFormatter::NONE . ',' . IntlDateFormatter::MEDIUM . ')'
    . '|' . self::TIME_LONG . '(*:' . IntlDateFormatter::NONE . ',' . IntlDateFormatter::LONG . ')'
    . '|' . self::TIME_FULL . '(*:' . IntlDateFormatter::NONE . ',' . IntlDateFormatter::FULL . ')'

    . '|' . self::DATE_SHORT . '\\W+' . self::TIME_SHORT . '(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::SHORT . ')'
    . '|' . self::DATE_MEDIUM . '\\W+' . self::TIME_SHORT . '(*:' . IntlDateFormatter::MEDIUM . ',' . IntlDateFormatter::SHORT . ')'
    . '|' . self::DATE_LONG . '\\W+' . self::TIME_SHORT . '(*:' . IntlDateFormatter::LONG . ',' . IntlDateFormatter::SHORT . ')'
    . '|' . self::DATE_FULL . '\\W+' . self::TIME_SHORT . '(*:' . IntlDateFormatter::FULL . ',' . IntlDateFormatter::SHORT . ')'

    . '|%c(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::MEDIUM . ')'
    . '|' . self::DATE_SHORT . '\\W+' . self::TIME_MEDIUM . '(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::MEDIUM . ')'
    . '|' . self::DATE_MEDIUM . '\\W+' . self::TIME_MEDIUM . '(*:' . IntlDateFormatter::MEDIUM . ',' . IntlDateFormatter::MEDIUM . ')'
    . '|' . self::DATE_LONG . '\\W+' . self::TIME_MEDIUM . '(*:' . IntlDateFormatter::LONG . ',' . IntlDateFormatter::MEDIUM . ')'
    . '|' . self::DATE_FULL . '\\W+' . self::TIME_MEDIUM . '(*:' . IntlDateFormatter::FULL . ',' . IntlDateFormatter::MEDIUM . ')'

    . '|' . self::DATE_SHORT . '\\W+' . self::TIME_LONG . '(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::LONG . ')'
    . '|' . self::DATE_MEDIUM . '\\W+' . self::TIME_LONG . '(*:' . IntlDateFormatter::MEDIUM . ',' . IntlDateFormatter::LONG . ')'
    . '|' . self::DATE_LONG . '\\W+' . self::TIME_LONG . '(*:' . IntlDateFormatter::LONG . ',' . IntlDateFormatter::LONG . ')'
    . '|' . self::DATE_FULL . '\\W+' . self::TIME_LONG . '(*:' . IntlDateFormatter::FULL . ',' . IntlDateFormatter::LONG . ')'

    . '|' . self::DATE_SHORT . '\\W+' . self::TIME_FULL . '(*:' . IntlDateFormatter::SHORT . ',' . IntlDateFormatter::FULL . ')'
    . '|' . self::DATE_MEDIUM . '\\W+' . self::TIME_FULL . '(*:' . IntlDateFormatter::MEDIUM . ',' . IntlDateFormatter::FULL . ')'
    . '|' . self::DATE_LONG . '\\W+' . self::TIME_FULL . '(*:' . IntlDateFormatter::LONG . ',' . IntlDateFormatter::FULL . ')'
    . '|' . self::DATE_FULL . '\\W+' . self::TIME_FULL . '(*:' . IntlDateFormatter::FULL . ',' . IntlDateFormatter::FULL . ')'

    . ')$#sD';

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('intl', 'boolean', "This will disable guessing the intl date format.", false, null);
    }

    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        // if intl isn't explicitly defined try to figure out if this extension is disabled
        if ($arguments['intl'] === null && $renderingContext instanceof RenderingContext) {
            $extensionName = $renderingContext->getControllerContext()->getRequest()->getControllerExtensionName();
            if ($extensionName) {
                $configuration = GeneralUtility::makeInstance(Configuration::class);
                $arguments['intl'] = !GeneralUtility::inList($configuration['disableInExtensions'], $extensionName);
            }
        }

        if (!$arguments['intl'] && $arguments['intl'] !== null) {
            return parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
        }

        $format = $arguments['format'] ?: $GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'] ?: 'Y-m-d';
        try {
            if (preg_match(self::EXPRESSION, $format, $match)) {
                [$dateType, $timeType] = explode(',', $match['MARK'], 2);
                return self::renderIntl($arguments, $renderChildrenClosure, $dateType, $timeType);
            }
        } catch (\Exception $e) {
            $logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
            $logger->warning("exception while useing intl for date format {$format}.", ['exception' => $e]);
            if (!GeneralUtility::getApplicationContext()->isProduction()) {
                throw $e;
            }
        }

        return parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
    }

    private static function renderIntl(array $arguments, \Closure $renderChildrenClosure, int $dateType, int $timeType): string
    {
        $date = self::prepareDate($arguments, $renderChildrenClosure);
        if ($date === null) {
            return '';
        }

        $locale = null;
        if (!$locale && self::getCurrentSiteLanguage()) {
            $locale = self::getCurrentSiteLanguage()->getLocale();
        }

        if (!$locale) {
            $locale = $GLOBALS['TSFE']->config['config']['locale_all'];
        }

        if (!$locale) {
            $locale = setlocale(LC_TIME, 0);
        }

        if (!is_string($locale)) {
            $type = is_object($locale) ? get_class($locale) : gettype($locale);
            throw new \RuntimeException("Locale missing, got $type");
        }

        $formatter = new IntlDateFormatter($locale, $dateType, $timeType);
        return $formatter->format($date);
    }

    /**
     * @return SiteLanguage|null
     */
    protected static  function getCurrentSiteLanguage()
    {
        // site language is typo3 9 only
        if (!interface_exists(ServerRequestInterface::class) || !class_exists(SiteLanguage::class)) {
            return null;
        }

        if (isset($GLOBALS['TYPO3_REQUEST'])
            && $GLOBALS['TYPO3_REQUEST'] instanceof ServerRequestInterface
            && $GLOBALS['TYPO3_REQUEST']->getAttribute('language') instanceof SiteLanguage) {
            return $GLOBALS['TYPO3_REQUEST']->getAttribute('language');
        }

        return null;
    }

    /**
     * This is basically a copy of the date preparation that the DateViewHelper normally does.
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     *
     * @return \DateTimeInterface|null
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     */
    private static function prepareDate(array $arguments, \Closure $renderChildrenClosure): ?\DateTimeInterface
    {
        if (class_exists(Context::class)) {
            $base = $arguments['base'] ?? GeneralUtility::makeInstance(Context::class)->getPropertyFromAspect('date', 'timestamp');
        } else {
            $base = $arguments['base'] ?? time();
        }
        if (is_string($base)) {
            $base = trim($base);
        }

        $date = $renderChildrenClosure();
        if ($date === null) {
            return null;
        }

        if (is_string($date)) {
            $date = trim($date);
        }

        if ($date === '') {
            $date = 'now';
        }

        if (!$date instanceof \DateTimeInterface) {
            try {
                $base = $base instanceof \DateTimeInterface ? $base->format('U') : strtotime((MathUtility::canBeInterpretedAsInteger($base) ? '@' : '') . $base);
                $dateTimestamp = strtotime((MathUtility::canBeInterpretedAsInteger($date) ? '@' : '') . $date, $base);
                $date = new \DateTime('@' . $dateTimestamp);
                $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            } catch (\Exception $exception) {
                throw new Exception('"' . $date . '" could not be parsed by \DateTime constructor: ' . $exception->getMessage(), 1241722579);
            }
        }

        return $date;
    }
}
