<?php

namespace Hn\Intl;


use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Configuration implements SingletonInterface, \ArrayAccess
{
    /**
     * @var array
     */
    private $conf;

    public function __construct()
    {
        $this->conf = class_exists(ExtensionConfiguration::class)
            ? GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('intl')
            : unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['intl']);
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->conf);
    }

    public function offsetGet($offset)
    {
        return $this->conf[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        throw new \LogicException("this method is not implemented.");
    }

    public function offsetUnset($offset)
    {
        throw new \LogicException("this method is not implemented.");
    }
}
