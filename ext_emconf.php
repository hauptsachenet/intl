<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Intl',
    'version' => '0.0.2',
    'description' => "This extension overrides the date view helper of typo3 and renders it using the php-intl functions.",
    'category' => 'fe',
    'author' => 'Marco Pfeiffer',
    'author_email' => 'marco@hauptsache.net',
    'author_company' => 'hauptsache.net',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.10-9.5.99',
            'fluid' => '*',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'autoload' => [
        'psr-4' => [
            'Hn\\Intl\\' => 'Classes',
        ],
    ],
];
