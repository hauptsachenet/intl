<?php

namespace Hn\Intl\Tests\Functional\ViewHelpers;


use Hn\Intl\Tests\Functional\FunctionalTestCase;
use TYPO3\CMS\Fluid\View\StandaloneView;

class DateViewHelperTest extends FunctionalTestCase
{
    private $oldConfig;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        date_default_timezone_set("Europe/Berlin");
    }

    protected function setUp()
    {
        parent::setUp();
        $this->oldConfig = $GLOBALS['TSFE']->config;
        $GLOBALS['TSFE']->config['config']['locale_all'] = 'en_US';
    }

    protected function tearDown()
    {
        $GLOBALS['TSFE']->config = $this->oldConfig;
        parent::tearDown();
    }

    public function testSimpleDate()
    {
        $view = new StandaloneView();
        $view->setTemplatePathAndFilename(__DIR__ . '/../../Fixtures/DateViewHelper.html');
        $view->assign('date', new \DateTime('02.01.1970 10:30:00'));
        $view->assign('format', 'd.m.Y');
        $this->assertEquals("Jan 2, 1970", trim($view->render()));

        $view->assign('format', 'dmY');
        $this->assertEquals("02011970", trim($view->render()));
    }
}
