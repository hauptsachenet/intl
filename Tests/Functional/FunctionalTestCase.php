<?php

namespace Hn\Intl\Tests\Functional;


use TYPO3\CMS\Core\Utility\GeneralUtility;

class FunctionalTestCase extends \Nimut\TestingFramework\TestCase\FunctionalTestCase
{
    protected $coreExtensionsToLoad = [
        'recordlist', // failure in php 7.2 for some reason
    ];

    protected $testExtensionsToLoad = [
        'typo3conf/ext/intl',
    ];

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        // for some reason this needs to be defined or else errors will just screw phpunit
        $GLOBALS['TYPO3_CONF_VARS']['LOG'] = [];
    }

    public static function tearDownAfterClass()
    {
        unset($GLOBALS['TYPO3_CONF_VARS']['LOG']);
        parent::tearDownAfterClass();
    }

    protected function setUp()
    {
        parent::setUp();
    }

    protected function tearDown()
    {
        GeneralUtility::purgeInstances();
        unset($GLOBALS['BE_USER']);
        parent::tearDown();
    }

}
