<?php

namespace Hn\Intl\Tests\Unit\ViewHelpers;


use Hn\Intl\ViewHelpers\DateViewHelper;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class DateViewHelperTest extends UnitTestCase
{
    /**
     * @var \DateTimeImmutable
     */
    protected static $date;

    public static function setUpEvenBeforeDataProvider()
    {
        date_default_timezone_set("Europe/Berlin");
        self::$date = new \DateTimeImmutable('02.01.1970 10:30:00');
    }

    public static function setUpBeforeClass()
    {
        self::setUpEvenBeforeDataProvider();
        parent::setUpBeforeClass();
    }

    protected function setUp()
    {
        parent::setUp();
        $GLOBALS['TSFE'] = new \stdClass();
        $GLOBALS['TSFE']->config['config']['locale_all'] = 'en_US';
    }

    protected function tearDown()
    {
        unset($GLOBALS['TSFE']);
        parent::tearDown();
    }

    public static function simpleCases()
    {
        self::setUpEvenBeforeDataProvider();

        $dateCases = [
            ['d.m.y', '1/2/70', \IntlDateFormatter::SHORT],
            ['m/d/y', '1/2/70', \IntlDateFormatter::SHORT],
            ['d.m.Y', 'Jan 2, 1970', \IntlDateFormatter::MEDIUM],
            ['m/d/Y', 'Jan 2, 1970', \IntlDateFormatter::MEDIUM],
            ['F d, Y', 'January 2, 1970', \IntlDateFormatter::LONG],
            ['d. F Y', 'January 2, 1970', \IntlDateFormatter::LONG],
            ['d F Y', 'January 2, 1970', \IntlDateFormatter::LONG],
            ['l d M Y', 'Friday, January 2, 1970', \IntlDateFormatter::FULL],
            ['l, M d, Y', 'Friday, January 2, 1970', \IntlDateFormatter::FULL],

            ['%B %e, %Y', 'January 2, 1970', \IntlDateFormatter::LONG], // format fluid_styled_content uses
            ['%B %e %Y', 'January 2, 1970', \IntlDateFormatter::LONG], // should give the same result as above
        ];

        $timeCases = [
            ['H:i', '10:30 AM', \IntlDateFormatter::SHORT],
            ['H:i:s', '10:30:00 AM', \IntlDateFormatter::MEDIUM],
            ['H:i:s T', '10:30:00 AM GMT+1', \IntlDateFormatter::LONG],
            ['H:i:s e', '10:30:00 AM Central European Standard Time', \IntlDateFormatter::FULL],
        ];

        $cases = [
            ['d', '02', null, null],
            ['d.m', '02.01', null, null],
            ['Y', '1970', null, null],
            ['c', self::$date->format('c'), null, null],
            ['r', self::$date->format('r'), null, null],
            ['u', self::$date->format('u'), null, null],
        ];

        foreach ($dateCases as $dateCase) {
            $cases[] = [$dateCase[0], $dateCase[1], $dateCase[2], null];
        }

        foreach ($timeCases as $timeCase) {
            $cases[] = [$timeCase[0], $timeCase[1], null, $timeCase[2]];
        }

        foreach ($dateCases as $dateCase) {
            foreach ($timeCases as $timeCase) {
                $combinator = $dateCase[2] <= \IntlDateFormatter::LONG ? ' at ' : ', ';
                $cases[] = [
                    "{$dateCase[0]}, {$timeCase[0]}",
                    "{$dateCase[1]}{$combinator}{$timeCase[1]}",
                    $dateCase[2],
                    $timeCase[2],
                ];
            }
        }

        return $cases;
    }

    /**
     * @dataProvider simpleCases
     */
    public function testFormatting($format, $expectedDate, $expectedDateFormat, $expectedTimeFormat)
    {
        $this->assertEquals($expectedDate, DateViewHelper::renderStatic(
            ['format' => $format, 'intl' => true, 'base' => null],
            static function () {
                return self::$date;
            },
            $this->createMock(RenderingContextInterface::class))
        );
    }

    /**
     * @dataProvider simpleCases
     */
    public function testIntlDisabled($format, $expectedDate, $expectedDateFormat, $expectedTimeFormat)
    {
        if (strpos($format, '%') !== false) {
            $expectedDate = strftime($format, self::$date->getTimestamp());
        } else {
            $expectedDate = self::$date->format($format);
        }
        $this->assertEquals($expectedDate, DateViewHelper::renderStatic(
            ['format' => $format, 'intl' => false, 'base' => null],
            static function () {
                return self::$date;
            },
            $this->createMock(RenderingContextInterface::class))
        );
    }
}
