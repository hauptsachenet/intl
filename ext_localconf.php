<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function ($extkey) {
    $conf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Hn\Intl\Configuration::class);

    if (!$conf['disableOverride']) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper::class]['className']
            = \Hn\Intl\ViewHelpers\DateViewHelper::class;
    }
}, $_EXTKEY);
