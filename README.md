[![Packagist Version](https://img.shields.io/packagist/v/hn/intl.svg)](https://packagist.org/packages/hn/intl)
[![Packagist](https://img.shields.io/packagist/l/hn/intl.svg)](https://packagist.org/packages/hn/intl)
[![Packagist](https://img.shields.io/packagist/dt/hn/intl.svg)](https://packagist.org/packages/hn/intl)
[![Packagist](https://img.shields.io/packagist/dm/hn/intl.svg)](https://packagist.org/packages/hn/intl)
[![Build status](https://img.shields.io/bitbucket/pipelines/hauptsachenet/intl/master.svg)](https://bitbucket.org/hauptsachenet/intl/addon/pipelines/home)

# Drop-In solution for better date formatting in TYPO3

This extension overrides the default date view helper to render the date using php-intl for internationalization.

## How does it work

You normally use the date view helper like this:

```html
<f:format:date date="now" format="d.m.Y" />
```

This would give you `21.01.2019` which is mostly fine
but this extension will parse the format string and figure out that you want a short date format
and by using the current sys_language will actually give you `Jan 21, 2019` (if your language is en).

This, however, can totally change based on that language. If you want to see the date in every locale possible run
`php -r 'use IntlDateFormatter as I; foreach (resourcebundle_locales("") as $locale) echo "$locale: ".(new I($locale, I::LONG, I::SHORT))->format(new DateTime("now"))."\n";'`
on your machine or the serer you plan to host on.

Depending on your format the guess will change, here are a few examples:

| format     | normal (based on the format) | intl (en)                                  | intl (de)                             |
|------------|------------------------------|--------------------------------------------|---------------------------------------|
| d.m.y      | 21.01.19                     | 6/21/19                                    | 27.06.19                              |
| d.m.Y      | 21.01.2019                   | Jan 21, 2019                               | 27.06.2019                            |
| d. F Y     | 21. January 2019             | January 21, 2019                           | 21. Januar 2019                       |
| l d. F Y   | Monday, January 21, 2019     | Monday, January 21, 2019                   | Montag, 21. Januar 2019               |
| H:i        | 10:30                        | 10:30 AM                                   | 10:30                                 |
| H:i:s      | 10:30:00                     | 10:30:00 AM                                | 10:30:00                              |
| H:i:s T    | 10:30:00 UTC                 | 10:30:00 AM GMT+1                          | 10:30:00 MEZ                          |
| H:i:s e    | 10:30:00 UTC                 | 10:30:00 AM Central European Standard Time | 10:30:00 Mitteleuropäische Normalzeit |
| d.m.Y H:i  | 21.01.2019 10:30             | Jan 21, 2019, 10:30 AM                     | 21.01.2019, 10:30                     |
| d. F Y H:i | 21. January 2019 10:30       | January 21, 2019 at 10:30 AM               | 21. Januar 2019 um 10:30              |

This table is just a list of examples. It basically only matters which character you use.

There are cases where the format is important. I tried to automatically detect those
so none readable formats like `Ymd` (without spaces), formats like `c` (ISO 8601), `r` (RFC 2882) and `u` (timestamp)
and partial formats like just `Y` (year) aren't touched. That means that this code will still works as expected:

```html
<time datetime="{date -> f:format.date(format: 'c')}">{date -> f:format.date(format: 'd. F Y H:i')}</time>
```

If you still need to disable the magic behavior you have 3 options:

1. You can disable this magic by passing `intl="0"` to the view helper:
   `<f:format:date date="now" format="d.m.Y" intl="0" />`
   This has the disadvantage that you now have a dependency to this extension since the attribute `intl` normally
   does not exist. However, it is a very fine way to tune it on a per case base.
2. You can disable it on an extension base in the intl extension configuration (key: disableInExtensions).
   If you integrate an extension which for some reason needs to run exactly as intended
   or already has proper language support build in: use this method.
3. You can disable the override of the view helpers all together.
   Use the intl extension configuration and check `disableOverride`.
   It is still possible to use this extension but you'll have to import the namespace and use the view helpers explicitly. 

## Why no separate view helper?

- You don't need to learn new view helpers, you'll ~~accidentally~~ automatically use better date formats.
- Existing extensions will be improved automatically without extra work.
- If you develop with this extension in mind:
  There is no hard requirement to this extension and can fallback to default rendering without extra effort.

## Run the tests

This project has some basic tests to ensure that it works in all typo3 versions described in the composer.json.
These tests are run by bitbucket and defined in `bitbucket-pipelines.yml`.

To run them locally, there are some composer scripts provided in this project.
Just clone the project, run `composer install` and then `composer db:start`, wait a few seconds, then `composer test`.
You can also run `composer test -- --filter TestCase` to run specific text classes/methods/datasets.

Here is a list of available commands:

- `composer db:start` will start a database using a docker command.
  You don't have to use it if you have a database available but you'll need to define the `typo3Database*` variables.
- `composer db:stop` unsurprisingly stops the database again... and removes it.
- `composer test` will run all available tests. If your first run fails then you might want to run `cc`.
- `composer test:unit` will just run the unit tests.
- `composer test:functional` will just run the functional tests.
- `composer cc` will remove some temp files. If your functional test fail for no apparat reason try this.
